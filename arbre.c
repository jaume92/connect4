//
//  arbre.c
//  Quatre en ratlla
//
//  Jaume Muntsant (1271258)

//  arbre.c conté les funcions per generar els arbres

#include <stdio.h>
#include <stdlib.h>

#include "arbre.h"
#include "minimax.h"
#include "joc.h"
#include "FuncH.h"


void copiaTauler(int tauler[N][N], int ptauler[N][N]){  //Realitzem la còpia d'un tauler a una altra ubicació
    int i, j;
    
    for (i=0; i<N; i++) {
        
        for (j=0; j<N; j++) {
            ptauler[i][j]=tauler[i][j];
        }
    }
}

int determinaFills(int tauler[N][N]){  //Determina el nombre de fills
    int contador=0,j;
    
    for(j=0;j<N;j++){
        
        if(tauler[0][j]==0) contador ++;
    }
    
    return contador;
}

Node *creaNode(int tauler[N][N], int num_tirada, int jugador){  //Crea un node
    int num_fills;
    Node *p=malloc(sizeof(Node));
    
    copiaTauler(tauler,p->tauler); 
    aplicaTirada(p->tauler,num_tirada, jugador, p);
    num_fills=determinaFills(p->tauler);
    p->n_fills=num_fills;
    p->fills=malloc(num_fills*sizeof(Node *));
    
    return p;
}



void crea1Nivell(Node *pare, int jugador){  //Crea un nivell
    int i;
    
    for(i=0; i<pare->n_fills; i++){
        pare->fills[i]=creaNode(pare->tauler,i,jugador);
    }
}

void creaArbre(Node *arrel){   //Crea l'arbre
    int i;
    
    crea1Nivell(arrel, ORD);
    
    for(i=0;i<arrel->n_fills;i++){
        crea1Nivell(arrel->fills[i], HUMA);
    }
}


void recorreArbre(Node *arrel){  //Pinta l'arbre per terminal
    int i,j;
    
    for(i=0;i<arrel->n_fills;i++){
        printf("%lf\n",arrel->fills[i]->valor);
        
        for(j=0;j<arrel->fills[i]->n_fills;j++){
            printf("  %lf\t", arrel->fills[i]->fills[j]->valor);
        }
        
        printf("%i\n",i);
    }
}


void esborraNode(Node *arrel, int fill){  //Destrueix els nodes fills
    int j;
    
    for(j=0;j<arrel->fills[fill]->n_fills;j++){
        free(arrel->fills[fill]->fills[j]->fills);
        free(arrel->fills[fill]->fills[j]);
    }
}


void esborraNivell(Node *arrel){  //Destrueix un nivell
    int i;
    
    for(i=0; i<arrel->n_fills; i++){
        esborraNode(arrel,i);
        free(arrel->fills[i]->fills);
        free(arrel->fills[i]);
    }
}


void esborraArbre(Node *arrel){ //Destrueix l'arbre
    esborraNivell(arrel);
    free(arrel->fills);
    free(arrel);
}


void arbreRecurrent(Node *arrel, int nivell, int fill){   //Realitza de manera recurrent arbres
    int i = 0,j = 0,mm=0;
    Node *subarrel = NULL;
    
    if(nivell<M){
        
        for(i=0;i<arrel->n_fills;i++){
            
            for(j=0;j<arrel->fills[i]->n_fills;j++){
                subarrel=creaNode(arrel->fills[i]->fills[j]->tauler, 0, HUMA);
                creaArbre(subarrel);
                arbreRecurrent(subarrel, nivell+1, j);
                
                    if ((nivell=!0)) {
                        mm=minimax(subarrel);
                        arrel->fills[i]->fills[j]->valor=subarrel->fills[mm]->valor;
                        esborraArbre(subarrel);
                    }
            }
        }
    }
}