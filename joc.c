//
//  joc.c
//  Quatre en ratlla
//
//  Jaume Muntsant (1271258)

//  joc.c cotnté les funcions relacionades amb el funcionament del joc

#include <stdio.h>
#include <stdlib.h>

#include "arbre.h"
#include "minimax.h"
#include "joc.h"
#include "FuncH.h"

extern int partida[N][N];
int clmn=0;


void iniciTauler(int tauler[N][N]){   //Inicialització del tauler
    int i,j;
    
    for (i=0; i<N; i++) {
        
        for (j=0; j<N; j++) {
            tauler[i][j]=BUIT;
        }
    }
}

void printaTauler(int tauler[N][N]){  //Dibuixem el tauler per pantalla
    int i,j;
    
    printf("Pulsa  a: <-, d: ->, l: tirar, e: sortir\n");
    
    if (clmn==-1) {
        clmn=clmn+N;
    }
    
    for(i=1;i<=N;i++){
        
        if (i==clmn%N+1) {
            printf(KGRN"(%i)"KWHT,i);
        }
        
        else{
            printf(KWHT"(%i)"KWHT,i);
        }
    }
    
    printf("\n");
    
    for (i=0; i<N; i++) {
        
        for (j=0; j<N; j++) {
            
      if(tauler[i][j]==BUIT && j!=clmn%N)
        printf(KWHT"[ ]" KWHT);
            
      if(tauler[i][j]==BUIT && j==clmn%N)
        printf(KGRN"[ ]" KWHT);
            
	  if(tauler[i][j]==HUMA)
	    printf(KRED "[X]" KWHT);
            
	  if(tauler[i][j]==ORD)
	    printf(KBLU "[O]" KWHT);
	}
        
        printf("\n");
    }
    
    for(i=1;i<=N;i++){
        
        if (i==clmn%N+1) {
            printf(KGRN"(%i)"KWHT,i);
        }
        
        else{
            printf(KWHT"(%i)"KWHT,i);
        }
    }
    
    printf("\n");
}


void taulerPle(){   //Determina si el tauler està ple, i finalitza la partida
    int i,k=0;
    
    for (i=0; i<N; i++) {
        
        if(determinaFila(partida, i)==-1)
            k++;
    }
    
    if (k==N) {
        printf("\nFi de la Partida: EMPAT\n");
        final();
    }
}


int fiPartida(int tauler[N][N]){  //Determina si s'ha relitzat un 4eR
    int i, j;
    
    //Horizontal
    for(i=0;i<N;i++){
        
        for(j=0;j<N-3;j++){
            
            if(tauler[i][j]!=BUIT && tauler[i][j]==tauler[i][j+1] && tauler[i][j+1]==tauler[i][j+2] && tauler[i][j+2]==tauler[i][j+3]){
                return 1;
            }
        }
    }
    
    //Vertical
    for(i=0;i<N-3;i++){
        
        for(j=0;j<N;j++){
            
            if(tauler[i][j]!=BUIT &&  tauler[i][j]==tauler[i+1][j] && tauler[i+1][j]==tauler[i+2][j] &&tauler[i+2][j]==tauler[i+3][j]){
                return 1;
            }
        }
    }
    
    //DiagonalU
    for(i=0;i<N-3;i++){
        
        for(j=3;j<N;j++){
            
            if(tauler[i][j]!=BUIT && tauler[i][j]==tauler[i+1][j-1] && tauler[i+1][j-1]==tauler[i+2][j-2] &&tauler[i+2][j-2]==tauler[i+3][j-3]){
                return 1;
            }
        }
    }
    
    //DiagonalD
    for(i=0;i<N-3;i++){
        
        for(j=0;j<N-3;j++){
            
            if(tauler[i][j]!=BUIT &&  tauler[i][j]==tauler[i+1][j+1] && tauler[i+1][j+1]==tauler[i+2][j+2] &&tauler[i+2][j+2]==tauler[i+3][j+3]){
                return 1;
            }
        }
    }
    
    return 0;
}


void aplicaTirada(int tauler[N][N], int num_tirada, int jugador, Node *p){  //Realitza la tirada i avalua la FH
    int columna, fila;
    
    columna=determinaColumna(tauler,num_tirada);
    fila=determinaFila(tauler,columna);
    
    tauler[fila][columna]=jugador;
                
    if (jugador==HUMA) {
        p->valor=FH(tauler,fila,columna);
    }
}


int determinaFila(int tauler[N][N], int columna){   //Determina a quina fila cau la fitxa
    int i;
    
    for (i=N-1; i>=0; i--) {
        
        if (tauler[i][columna]==BUIT) {
            return i;
        }
    }
    
    return -1;
}


int determinaColumna(int tauler[N][N], int num_tirada){ //Classifica columnes plenes i buides
  int i,j=0;

        for (i=0; i<N; i++) {
            
            if(tauler[0][i]==BUIT)
                j++;
            
            if(j==num_tirada+1)
                return i;
        }
    
    return -1;
}


void tiraFitxaH(){  //Demanem al jugador tirar la fitxa
    int tirada;
    
    intro();
    tirada=clmn%N;
    
    if (partida[0][tirada]==BUIT && tirada>=0 && tirada<N) {
        partida[determinaFila(partida, tirada)][tirada]=HUMA;
    }
    
    else{
         printf("Error: Columna plena\n"); tiraFitxaH();
     }
}


void final(){    //Finalitza el joc i reestableix les opcions per defecte del terminal
    char resp;
    
    printf("\nGràcies per jugar!\n\n");
    printf("\nEntra qualsevol caràcter + intro per sortir \n");
    scanf("%c",&resp);
    printf(FI);
    printf(NETEJA);
    exit(0);
}

void intro(){     //Selecció de columna
    char entrada;

    while (1) {
        system ("/bin/stty raw");
        scanf("%c",&entrada);
        
        if (entrada=='d') {
            clmn++;
            system ("/bin/stty cooked");
            system("clear");
            
            while (determinaFila(partida, clmn%N)==-1) {
                clmn=clmn+1;
            }
            
            printaTauler(partida);
        }
        
        if (entrada=='a') {
            clmn--;
            system ("/bin/stty cooked");
            system("clear");
            
            while (determinaFila(partida, clmn%N)==-1) {
                clmn=clmn-1;
            }
            
            printaTauler(partida);
        }
        
        if (entrada=='l')
            break;
        
        if (entrada=='e') {
            system ("/bin/stty cooked");
            final();
            exit(0);
        }
    }
    
    system ("/bin/stty cooked");
}

int contaBuit(int tauler[N][N]){ //Conta els espais blancs en un tauler
    int i,j, buit=0;
    
    for (i=0; i<N; i++) {
        
        for (j=0; j<N; j++) {
            
            if(tauler[i][j]==BUIT)
                buit++;
        }
    }
    
    return buit;
}














