//
//  minimax.h
//  Quatre en ratlla
//
//  Jaume Muntsant (1271258)

double buscaMinim(Node *arrel, int i);
int max(Node *arrel);
void mini(Node *arrel);
int minimax(Node *arrel);

