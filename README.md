# Quatre en ratlla #

Es tracta d'un projecte que vaig fer per l'assignatura de programació avançada el 2015.

### Per jugar ###

**1.** Posar ``make`` al terminal per compilar.

**2.** Executar amb ``./4eR``.

**3.** Jugar!

![Captura de pantalla 2016-11-06 a les 11.10.09.png](https://bitbucket.org/repo/4beLkK/images/2771291137-Captura%20de%20pantalla%202016-11-06%20a%20les%2011.10.09.png)