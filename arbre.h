//
//  arbre.h
//  Quatre en ratlla
//
//  Jaume Muntsant (1271258)

#define BUIT 0
#define HUMA 1
#define ORD 2

#define N 8         //Nombre de files i columnes del tauler
#define MX -10000   //Valor màxim al produir-se un 4eR
#define M 1         //Nombre de nivell(0-> 1 nivell, 1-> 2 nivells)

#define TERM "\e[37m\e[40m"   //Opcions per donar format al terminal (Color de lletra i fons)
#define FI "\e[0m"
#define NETEJA "\e[2J\e[1;1H"

#define KNRM  "\x1B[0m"   //Definim alguns colors de font
#define KRED  "\x1B[31m"
#define KGRN  "\x1B[32m"
#define KYEL  "\x1B[33m"
#define KBLU  "\x1B[34m"
#define KMAG  "\x1B[35m"
#define KCYN  "\x1B[36m"
#define KWHT  "\x1B[37m"

typedef struct node{
    struct node **fills;
    int n_fills;
    int tauler[N][N];
    double valor;
} Node;


void copiaTauler(int tauler[N][N], int ptauler[N][N]);
int determinaFills(int tauler[N][N]);
Node *creaNode(int tauler[N][N], int num_tirada, int jugador);
void crea1Nivell(Node *pare, int jugador);
void creaArbre(Node *arrel);
void recorreArbre(Node *arrel);
void esborraNode(Node *arrel, int fill);
void esborraNivell(Node *arrel);
void esborraNivell(Node *arrel);
void esborraArbre(Node *arrel);
void arbreRecurrent(Node *arrel, int num_arbre, int fill);