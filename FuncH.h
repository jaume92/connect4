//
//  FuncH.h
//  Quatre en ratlla
//
//  Jaume Muntsant (1271258)

int contaRectangle(int rectangle[4]);
int validaRectangle(int coord_rectangle[2]);
void iniciRectangle(int rectangle[4]);
int comprovaHor(int tauler[N][N], int fila, int columna);
int comprovaVer(int tauler[N][N], int fila, int columna);
int comprovaDD(int tauler[N][N], int fila, int columna);
int comprovaDU(int tauler[N][N], int fila, int columna);
int FH(int tauler[N][N], int fila, int columna);