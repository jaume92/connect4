//
//  minimax.c
//  Quatre en ratlla
//
//  Jaume Muntsant (1271258)

// Conté les funcions necessàries per realitzar el minimax

#include <stdio.h>
#include <stdlib.h>

#include "arbre.h"
#include "minimax.h"
#include "joc.h"
#include "FuncH.h"


double buscaMinim(Node *arrel, int i){   //Retorna el mínim dels valors
    int k;
    double minim;
    
    minim=arrel->fills[i]->fills[0]->valor;
    
    for (k = 1; k<arrel->fills[i]->n_fills; k++) {
        
        if (arrel->fills[i]->fills[k]->valor < minim)
            minim = arrel->fills[i]->fills[k]->valor;
    }
    
    return minim;
}

int max(Node *arrel){  //Retorna el màxims dels valors
    int k, posicio=0;
    double maxim;
    
    maxim=arrel->fills[0]->valor;
    
    for (k = 1; k<arrel->n_fills; k++) {
        
        if (arrel->fills[k]->valor > maxim){
            maxim= arrel->fills[k]->valor;
            posicio=k;
        }
    }
    
    return posicio;
}


void mini(Node *arrel){  //Col·loca el mínim en el pare
    int i;
    
    for(i=0;i<arrel->n_fills;i++){
        arrel->fills[i]->valor=buscaMinim(arrel,i);
    }
}


int minimax(Node *arrel){ //Realitza el minimax
    int posicio;
    
    mini(arrel);
    posicio=max(arrel);
    
    return  posicio;
}
