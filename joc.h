//
//  joc.h
//  Quatre en ratlla
//
//  Jaume Muntsant (1271258)

void iniciTauler(int tauler[N][N]);
void printaTauler(int tauler[N][N]);
void taulerPle();
int fiPartida(int tauler[N][N]);
void aplicaTirada(int tauler[N][N], int num_tirada, int jugador, Node *p);
int determinaFila(int tauler[N][N], int columna);
int determinaColumna(int tauler[N][N], int num_tirada);
void tiraFitxaH();
void final();
void intro();
int contaBuit(int tauler[N][N]);