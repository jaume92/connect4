//
//  FuncH.c
//  Quatre en ratlla
//
//  Jaume Muntsant (1271258)

//  FuncH.c conté les funcions relacionades amb la funció heurística

#include <stdio.h>
#include <stdlib.h>

#include "arbre.h"
#include "minimax.h"
#include "joc.h"
#include "FuncH.h"


int contaRectangle(int rectangle[4]){     //Funció que associa un valor al rectangle
    int i,suma=0;
    
    for(i=0;i<4;i++){
        
        if (rectangle[i]==ORD)
            return 0;
        if(rectangle[i]==HUMA)
            suma++;
    }
    
    switch (suma){
        case 0:
            return 0;
        case 1:
            return -1;
        case 2:
            return -4;
        case 3:
            return -9;
        case 4:
            return MX;
    }
    
    return 0;
}


int validaRectangle(int coord_rectangle[2]){   //Funció que comprova que el rectangle creat estigui al tauler
    int i;
    
    for (i=0; i<2; i++){
        
        if(coord_rectangle[i]<0 || coord_rectangle[i]>=N)
            return 0;
    }
    
    return 1;
}


void iniciRectangle(int rectangle[4]){   //Inicialització del rectangle
    int k;
    
    for(k=0;k<4;k++){
        rectangle[k]=0;
    }
}


int comprovaHor(int tauler[N][N], int fila, int columna){    //Comprovem a partir d'una coordenada la possibilitat d'un 4eR horizontal
    int i,j, contador=0;
    int rectangle[4];
    int coord_rectangle[2];
    
    iniciRectangle(rectangle);
    
    for(j=columna-3; j<=columna; j++){
        
        for(i=0; i<4;i++){
            coord_rectangle[0]=fila;
            coord_rectangle[1]=j+i;
            
            if(validaRectangle(coord_rectangle)==0){
                iniciRectangle(rectangle);
                break;
            }
            
            else{
                rectangle[i]=tauler[fila][j+i];
            }
        }
        
        contador=contador+contaRectangle(rectangle);
    }
    
    return contador;
}


int comprovaVer(int tauler[N][N], int fila, int columna){  //Comprovem a partir d'una coordenada la possibilitat d'un 4eR vertical
    int i,j, contador=0;
    int rectangle[4];
    int coord_rectangle[2];
    
    iniciRectangle(rectangle);
    
    for(j=fila-3; j<=fila; j++){
        
        for(i=0; i<4;i++){
            coord_rectangle[0]=j+i;
            coord_rectangle[1]=columna;
            
            if(validaRectangle(coord_rectangle)==0){
                iniciRectangle(rectangle);
                break;
            }
            
            else{
                rectangle[i]=tauler[j+i][columna];
            }
        }
        
        contador=contador+contaRectangle(rectangle);
    }
    
    return contador;
}


int comprovaDD(int tauler[N][N], int fila, int columna){  //Comprovem a partir d'una coordenada la possibilitat d'un 4eR diagonal desc.
    int i,j,k, contador=0;
    int rectangle[4];
    int coord_rectangle[2];
    
    iniciRectangle(rectangle);
    
    for(j=fila-3, k=columna-3; k<=columna; j++,k++){
        
        for(i=0; i<4;i++){
            coord_rectangle[0]=j+i;
            coord_rectangle[1]=k+i;
            
            if(validaRectangle(coord_rectangle)==0){
                iniciRectangle(rectangle);
                break;
            }
            
            else{
                rectangle[i]=tauler[j+i][k+i];
            }
        }
        
        contador=contador+contaRectangle(rectangle);
    }
    
    return contador;
}


int comprovaDU(int tauler[N][N], int fila, int columna){  //Comprovem a partir d'una coordenada la possibilitat d'un 4eR diagonal asc.
    int i,j,k, contador=0;
    int rectangle[4];
    int coord_rectangle[2];

    iniciRectangle(rectangle);
    
    for(j=fila+3, k=columna-3; k<=columna; j--,k++){
        
        for(i=0; i<4;i++){
            coord_rectangle[0]=j-i;
            coord_rectangle[1]=k+i;
            
            if(validaRectangle(coord_rectangle)==0){
                iniciRectangle(rectangle);
                break;
            }
            
            else{
                rectangle[i]=tauler[j-i][k+i];
            }
        }
        
        contador=contador+contaRectangle(rectangle);
    }
    
    return contador;
}


int FH(int tauler[N][N], int fila, int columna){    //Retornem el resultat de FH a partir de tots els rectangles
    int resultat = 0;
    
    resultat=comprovaHor(tauler, fila, columna)+comprovaVer(tauler, fila, columna)+comprovaDD(tauler, fila, columna)+comprovaDU(tauler,fila, columna);

    return resultat;
}