//
//  main.c
//  Quatre en ratlla
//
//  Jaume Muntsant (1271258)

#include <stdio.h>
#include <stdlib.h>

#include "arbre.h"
#include "minimax.h"
#include "joc.h"
#include "FuncH.h"

int partida[N][N];    //Tauler on guardem la partida


int main(){
    int mm = 0, i, arbre=0;
    int suport[N][N];       //Tauler de suport per aplicar tirades
    
    Node *arrel;
    Node *tauler_suport;

    iniciTauler(partida);
    arrel=creaNode(partida, 0, HUMA);
    
    system("clear");    //Missatge de benvinguda
    printf(TERM);
    printf(NETEJA);
    printf(KBLU"OOO"KRED"XXX"KBLU"OOO"KRED"XXX"KBLU"OOO"KRED"XXX"KBLU"OOO"KRED"XXX"KBLU"OOO"KRED"XXX\n"KWHT);
    printf("Benvingut al Quatre en Ratlla!\n");
    printf(KBLU"OOO"KRED"XXX"KBLU"OOO"KRED"XXX"KBLU"OOO"KRED"XXX"KBLU"OOO"KRED"XXX"KBLU"OOO"KRED"XXX\n"KWHT);
    printf("\nJugaràs amb les '"KRED"X"KWHT"'\n\n");
    
    printaTauler(partida);      //Dibuixem el tauler inicial
    
    //Bucle on s'esdevé la partida
    while(1){
        
        if(fiPartida(partida)!=1){
            tiraFitxaH();             //Demanem a l'humà tirar la fitxa
            system("clear");
            
            if(fiPartida(partida)!=1){
                arrel=creaNode(partida, 0, HUMA);
                copiaTauler(partida, arrel->tauler);
                copiaTauler(partida, suport);
                
                //Força a l'ordinador a guanyar si pot fer 4eR
                for (i=0; i<N; i++) {
                    aplicaTirada(suport, i, ORD, tauler_suport);
                    
                    if (fiPartida(suport)) {     //Missatge de victòria de l'ordinador
                        printaTauler(suport);
                        printf("\nHas perdut: Guanya "KBLU"OOO ORDINADOR OOO\n"KWHT);
                        final();
                    }
                    
                    copiaTauler(partida, suport);
                }
                
                //Última tirada
                if (contaBuit(partida)==1) {
                    
                    for (i=0; i<N; i++) {
                        
                        if(determinaFila(partida, i)==0)
                            partida[0][i]=ORD;
                        
                        system("clear");
                        printaTauler(partida);
                        taulerPle();
                        }
                    }
                
                creaArbre(arrel);
                mm=minimax(arrel);
                
                //Determinem quants nivells creem
                for (i=0; i<arrel->n_fills; i++) {
                    
                    if (arrel->fills[i]->valor<MX) {
                        arbre=0;
                        break;
                    }
                    
                    else {
                        arbre=1;
                    }
                }
                
                //Penúltima tirada
                if (contaBuit(partida)==3) {
                    arbre=0;
                }
                
                //Creem dos nivells d'arbres
                if (arbre==1) {
                    arbreRecurrent(arrel, 0, 0);
                    mm=minimax(arrel);
                }
                
                copiaTauler(arrel->fills[mm]->tauler, partida);      //Copiem el tauler més óptim a partida
                
                printaTauler(partida);
                
                esborraArbre(arrel);     //Netejem l'arbre superior
            }
            
            else break;
        }
        
        else break;
    }
    
    printaTauler(partida);     //Missatge de victòria de l'humà
    printf("\nHas guanyat! "KRED"XXX FELICITATS XXX"KWHT);
    final();
    
    return 0;
}



